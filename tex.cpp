// illustrates how to keep the smallest 
// 4 ints from a big unordered list

#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;


    
#define REP(i, a, b) \
    for (int i = int(a); i <= int(b); i++)



int main()
{
    char c;
    char j = 0;
    string st;
    while(getline (std::cin,st) ) {
        for (int i=0; i<st.length(); i++) {  
             if (st[i]=='\"') {
            j++;
            if (j%2) cout << "``";
            else     cout << "\'\'";
        }
        else cout << st[i];
      } 
      cout << endl;
    }
    return 0;
}


