// illustrates how to keep the smallest 
// 4 ints from a big unordered list

#include <iostream>
#include <queue>
#include <vector>
#include <cstdlib>
using namespace std;
typedef vector<int> vi;
typedef priority_queue<int> maxpqi;
typedef priority_queue<int, std::vector<int>, std::greater<int> > minpqi;

#define TRvi(c, it) \
    for (vi::iterator it = (c).begin(); it != (c).end(); it++)
#define TRpqi(c, it) \
    for (maxpqi::iterator it = (c).begin(); it != (c).end(); it++)
#define WNEpqi(c) \
    for (;!(c).empty(); (c).pop())
    
#define REP(i, a, b) \
    for (int i = int(a); i <= int(b); i++)

int main()
{
   maxpqi first;
    
   srand(time(NULL));
   
   REP(i,0,1000000) {
       int j = rand()%1000000;
       if (i < 4) first.push(j);
       else if (j < first.top()) {
           first.pop();
           first.push(j);
       }
   }
   WNEpqi(first) {cout << first.top() << endl;}
   
   return 0;
}

