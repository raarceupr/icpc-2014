// illustrates how to keep the smallest 
// 4 longs from a big unordered list

#include <iostream>

#include <cstdlib>
using namespace std;


    
#define REP(i, a, b) \
    for (long i = long(a); i <= long(b); i++)


unsigned long foo(unsigned long n) {
    long ctr = 1;
    while (n != 1) {
        if (n%2) n = 3*n + 1;
        else     n = n/2;
        ctr++;
    }    
    return ctr;
}

int main()
{
    unsigned long a, b;
    unsigned long m = 1;
    unsigned long r;
    bool swapped = false;
    while ( cin >> a >> b) {
		swapped = false;
    	if (b<a) { swap(a,b); swapped = true; }
        m = 0;
        for(unsigned long i=a; i<=b; i++)  {
            r = foo(i);
            m = r > m ? r : m; 
        }
        if (swapped) swap(a,b); 
        cout << a << " " << b << " " << m << endl;
    }
    return 0;
}


